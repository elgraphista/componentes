/* Sala */

const chat = document.getElementById('chat');
const reporte = document.getElementById('reporte');

const chatM = document.getElementById('chat-m');
const reporteM = document.getElementById('reporte-m');
const closePanel = document.getElementById('close-panel')

const panel = document.getElementById('panel')
const panelM = document.getElementById('panel-m')
const closePanelM = document.getElementById('close-panel-m')



// Func Desk

const showPanel = () => { 
    panel.classList.toggle('show-panel') 
}
const removePanel = () => { 
    panel.classList.remove('show-panel') 
}

// Func Mobile

const showPanelM = () => { 
    panelM.classList.toggle('show-panel') 
}
const removePanelM = () => { 
    panelM.classList.remove('show-panel') 
}



chat.addEventListener('click', showPanel);
reporte.addEventListener('click', showPanel);
closePanel.addEventListener('click', removePanel);

chatM.addEventListener('click', showPanelM);
reporteM.addEventListener('click', showPanelM);
closePanelM.addEventListener('click', removePanelM);
